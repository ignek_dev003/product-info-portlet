<%@page import="com.liferay.petra.string.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ignek.sample.portlet.configuration.ProductInformationPortletConfiguration"%>
<%@page import="com.ignek.sample.model.ProductInformationModel"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/frontend" prefix="liferay-frontend" %>
<%@ taglib uri="http://liferay.com/tld/ddm" prefix="liferay-ddm" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	ProductInformationPortletConfiguration productInformationPortletConfiguration =
	        (ProductInformationPortletConfiguration)
	        renderRequest.getAttribute(ProductInformationPortletConfiguration.class.getName());
	
	
	String displayStyle = StringPool.BLANK;
	long displayStyleGroupId = 0;
	
	if (Validator.isNotNull(productInformationPortletConfiguration)) {
		displayStyle = portletPreferences.getValue("displayStyle", productInformationPortletConfiguration.displayStyle());
		displayStyleGroupId = GetterUtil.getLong(portletPreferences.getValue("displayStyleGroupId", null), productInformationPortletConfiguration.displayStyleGroupId());
	}
%>