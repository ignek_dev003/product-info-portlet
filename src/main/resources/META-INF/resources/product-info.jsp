<%@ include file="/init.jsp" %>

<%
    List<ProductInformationModel> productInformationList = (List<ProductInformationModel>)request.getAttribute("productInformationList");
%>

<c:if test="${not empty productInformationList}">
    
    <liferay-ddm:template-renderer
    className="<%= ProductInformationModel.class.getName() %>"
    displayStyle="<%= displayStyle %>"
    displayStyleGroupId="<%= displayStyleGroupId %>"
    entries="<%= productInformationList %>"
	>
		<h1><liferay-ui:message key="select.adt.from.portlet.configuration" /></h1>
	</liferay-ddm:template-renderer>

</c:if>