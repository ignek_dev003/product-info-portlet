<#if entries?has_content> 

    <#list entries as curProductInfo> 
        <ul>
        <div><strong>Product Name : </strong> <span>${curProductInfo.productName}</span> </div>
        <div><strong>Product Code : </strong>  <span>${curProductInfo.productCode}</span> </div>
        <div><strong>Product Detail : </strong> <span>${curProductInfo.productDetail}</span> </div>
        
        <#if curProductInfo.parts?has_content> 

            <#list curProductInfo.parts as part> 
               <li>  
                    <div><strong>Part Id : </strong> <span>${part.partId}</span> </div>
                    <div><strong>Part Name : </strong>  <span>${part.partName}</span> </div>
                    <div><strong>Part Info : </strong> <span>${part.partInfo}</span> </div>
               </li>
            </#list> 
    
        </#if>
        </ul>
    </#list> 
    
</#if>