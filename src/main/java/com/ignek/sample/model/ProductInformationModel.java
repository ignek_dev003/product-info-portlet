package com.ignek.sample.model;

import java.util.List;

/** provide model implementation for product
 * 
 * @author bhavin.panchani
 */
public class ProductInformationModel {

	private String productName;
	private String productCode;
	private String productDetail;
	private List<PartModel> parts;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductDetail() {
		return productDetail;
	}
	public void setProductDetail(String productDetail) {
		this.productDetail = productDetail;
	}
	
	
	public List<PartModel> getParts() {
		return parts;
	}
	public void setParts(List<PartModel> parts) {
		this.parts = parts;
	}
	@Override
	public String toString() {
		return "ProductInformationModel [productName=" + productName + ", productCode=" + productCode
				+ ", productDetail=" + productDetail + ", parts=" + parts + "]";
	}
	
}
