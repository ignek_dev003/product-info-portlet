package com.ignek.sample.model;

/** provide model implementation for part
 * 
 * @author bhavin.panchani
 */
public class PartModel {

	private String partId;
	private String partName;
	private String partInfo;
	
	public String getPartId() {
		return partId;
	}
	public void setPartId(String partId) {
		this.partId = partId;
	}
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}
	public String getPartInfo() {
		return partInfo;
	}
	public void setPartInfo(String partInfo) {
		this.partInfo = partInfo;
	}
	
	@Override
	public String toString() {
		return "PartModel [partId=" + partId + ", partName=" + partName + ", partInfo=" + partInfo + "]";
	}
	
	
}
