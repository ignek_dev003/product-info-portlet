package com.ignek.sample.portlet.template;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.ignek.sample.constants.ProductInformationPortletKeys;
import com.ignek.sample.model.ProductInformationModel;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.portletdisplaytemplate.BasePortletDisplayTemplateHandler;
import com.liferay.portal.kernel.portletdisplaytemplate.PortletDisplayTemplateManager;
import com.liferay.portal.kernel.template.TemplateHandler;
import com.liferay.portal.kernel.template.TemplateVariableGroup;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.ResourceBundleUtil;

/** Template handler class for product information portlet
 * 
 * @author bhavin.panchani
 */
@Component(
	immediate = true, 
	property = "javax.portlet.name=" + ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_ID,
	service = TemplateHandler.class
)
public class ProductInformationPortletDisplayTemplateHandler extends BasePortletDisplayTemplateHandler {

	@Override
	public String getClassName() {
		return ProductInformationModel.class.getName();
	}

	@Override
	public String getName(Locale locale) {
		ResourceBundle resourceBundle = ResourceBundleUtil.getBundle(
				"content.Language", locale, getClass());

		String portletTitle = _portal.getPortletTitle(
			ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_ID, resourceBundle);

		return LanguageUtil.format(locale, "x-template", portletTitle, false);
	}

	@Override
	public String getResourceName() {
		return ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_ID;
	}
	
	@Override
	public Map<String, TemplateVariableGroup> getTemplateVariableGroups(
			long classPK, String language, Locale locale)
		throws Exception {

		Map<String, TemplateVariableGroup> templateVariableGroups =
			super.getTemplateVariableGroups(classPK, language, locale);

		TemplateVariableGroup fieldsTemplateVariableGroup =
			templateVariableGroups.get("fields");

		fieldsTemplateVariableGroup.empty();
        
		// product information will be stored as the entries
		fieldsTemplateVariableGroup.addCollectionVariable(
          "productionInformations", List.class, PortletDisplayTemplateManager.ENTRIES,
          "productionInformation", ProductInformationModel.class, "curProductInfo", "productName");

		return templateVariableGroups;
	}

	@Reference
	private Portal _portal;
}
