package com.ignek.sample.portlet.configuration.action;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.ignek.sample.constants.ProductInformationPortletKeys;
import com.ignek.sample.portlet.configuration.ProductInformationPortletConfiguration;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

import aQute.bnd.annotation.metatype.Configurable;

/** Production Information Portlet configuration action class
 * 
 * @author bhavin.panchani
 */
@Component(
    configurationPid = ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_CONFIGURATION_ID,
    configurationPolicy = ConfigurationPolicy.OPTIONAL,
    immediate = true,
    property = {
        "javax.portlet.name="+ ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_ID
    },
    service = ConfigurationAction.class
)
public class ProductInformationPortletConfigurationAction extends DefaultConfigurationAction{
	
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		// TODO Auto-generated method stub
		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws Exception {
		httpServletRequest.setAttribute(ProductInformationPortletConfiguration.class.getName(), productInformationPortletConfiguration);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}
	
	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		productInformationPortletConfiguration = Configurable.createConfigurable(ProductInformationPortletConfiguration.class, properties);
	}
	
	private volatile ProductInformationPortletConfiguration productInformationPortletConfiguration;

}
