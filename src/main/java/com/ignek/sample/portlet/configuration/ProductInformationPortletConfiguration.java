package com.ignek.sample.portlet.configuration;

import com.ignek.sample.constants.ProductInformationPortletKeys;

import aQute.bnd.annotation.metatype.Meta;

/** Production Information Portlet configuration attributes
 * 
 * @author bhavin.panchani
 */
@Meta.OCD(id = ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_ID)
public interface ProductInformationPortletConfiguration {

	@Meta.AD(deflt = "", name = "displayStyle", required = false)
	public String displayStyle();

	@Meta.AD(deflt = "0", name = "displayStyleGroupId", required = false)
	public long displayStyleGroupId();
}
