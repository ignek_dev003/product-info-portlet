package com.ignek.sample.portlet.util;

import java.util.ArrayList;
import java.util.List;

import com.ignek.sample.model.PartModel;
import com.ignek.sample.model.ProductInformationModel;

/** Provides utility methods for product information portlet
 * 
 * @author bhavin.panchani
 */
public class ProductInformationPortletUtil {
	
	// populating mock list of production information
	public static List<ProductInformationModel> populateProductInformationModelList() {
		
		List<ProductInformationModel> productInformationList = new ArrayList<ProductInformationModel>();
		
		ProductInformationModel product1 = new ProductInformationModel();
		product1.setProductCode("prod001");
		product1.setProductDetail("product details 1");
		product1.setProductName("Product 1");
		
		List<PartModel> product1PartList = new ArrayList<PartModel>();
		
		PartModel partModel1 = new PartModel();
		partModel1.setPartId("product1 part1");
		partModel1.setPartInfo("Product1 part1 Information");
		partModel1.setPartName("Product1 Part1 Name");
		
		PartModel partModel2 = new PartModel();
		partModel2.setPartId("product1 part2");
		partModel2.setPartInfo("Product1 part2 Information");
		partModel2.setPartName("Product1 Part2 Name");
		
		product1PartList.add(partModel1);
		product1PartList.add(partModel2);
		
		product1.setParts(product1PartList);
		
		
		ProductInformationModel product2 = new ProductInformationModel();
		product2.setProductCode("prod002");
		product2.setProductDetail("product details 2");
		product2.setProductName("Product 2");
		
		List<PartModel> product2PartList = new ArrayList<PartModel>();
		
		partModel1 = new PartModel();
		partModel1.setPartId("product2 part1");
		partModel1.setPartInfo("Product2 part1 Information");
		partModel1.setPartName("Product2 Part1 Name");
		
		partModel2 = new PartModel();
		partModel2.setPartId("product2 part2");
		partModel2.setPartInfo("Product2 part2 Information");
		partModel2.setPartName("Product2W Part2 Name");
		
		product2PartList.add(partModel1);
		product2PartList.add(partModel2);
		
		product2.setParts(product2PartList);
		
		productInformationList.add(product1);
		productInformationList.add(product2);
		return productInformationList;
		
	}
}
