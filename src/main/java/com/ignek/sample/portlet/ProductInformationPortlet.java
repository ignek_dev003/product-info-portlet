package com.ignek.sample.portlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;

import com.ignek.sample.constants.ProductInformationPortletKeys;
import com.ignek.sample.model.ProductInformationModel;
import com.ignek.sample.portlet.configuration.ProductInformationPortletConfiguration;
import com.ignek.sample.service.ProductInformationService;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import aQute.bnd.annotation.metatype.Configurable;

/** handle all requests for product information portlet
 * 
 * @author bhavin.panchani
 */
@Component(
	immediate = true,
	configurationPid = ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_CONFIGURATION_ID,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/product-info.jsp",
		"javax.portlet.name=" + ProductInformationPortletKeys.PRODUCTION_INFORMATION_PORTLET_ID,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ProductInformationPortlet extends MVCPortlet {
	
	private volatile ProductInformationPortletConfiguration productInformationPortletConfiguration;
	
	private ProductInformationService productInformationService;
	
	@Reference
	public void setProductInformationService(ProductInformationService productInformationService) {
		this.productInformationService = productInformationService;
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		// fetch list of product information
		List<ProductInformationModel> productInformationList = productInformationService.getProductDetail();
		
		
		// adding product information list in request
		renderRequest.setAttribute(
				"productInformationList",
				productInformationList);
		
		// adding product information portlet configuration object in request
		renderRequest.setAttribute(
				ProductInformationPortletConfiguration.class.getName(),
				productInformationPortletConfiguration);
		
		super.render(renderRequest, renderResponse);

	}
	
	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		productInformationPortletConfiguration = Configurable.createConfigurable(
				ProductInformationPortletConfiguration.class, properties);
	}
}