package com.ignek.sample.service.impl;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.ignek.sample.model.ProductInformationModel;
import com.ignek.sample.portlet.util.ProductInformationPortletUtil;
import com.ignek.sample.service.ProductInformationService;


/** service implementation class for product information portlet
 * 
 * @author bhavin.panchani
 */
@Component(immediate = true, service = ProductInformationService.class)
public class ProductInformationServiceImpl implements ProductInformationService {

	// fetch production information list
	@Override
	public List<ProductInformationModel> getProductDetail() {
		
		// make a service call from here to get production information.
		// As we don't have service now, I returning mock objects 
		return ProductInformationPortletUtil.populateProductInformationModelList();
	}

}
