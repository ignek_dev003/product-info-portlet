package com.ignek.sample.service;

import java.util.List;

import com.ignek.sample.model.ProductInformationModel;
/** service interface for product information portlet
 * 
 * @author bhavin.panchani
 */
public interface ProductInformationService {

	public List<ProductInformationModel> getProductDetail();
}
