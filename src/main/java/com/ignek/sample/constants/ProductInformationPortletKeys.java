package com.ignek.sample.constants;

/** provide constants for Production Information Portlet
 * 
 * 
 * @author bhavin.panchani
 */
public class ProductInformationPortletKeys {

	/** Portlet id for Product Information Portlet **/
	public static final String PRODUCTION_INFORMATION_PORTLET_ID = "com_ignek_sample_portlet_ProductInformationPortlet";
	
	/** Configuration id for Product Information Portlet **/
	public static final String PRODUCTION_INFORMATION_PORTLET_CONFIGURATION_ID = "com_ignek_sample_portlet_configuration_ProductInformationPortletConfiguration";
	
}